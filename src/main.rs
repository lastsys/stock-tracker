use chrono::{DateTime, Utc};
use clap::Clap;
use yahoo_finance_api as yahoo;
use yahoo_finance_api::{YahooConnector, YahooError};

// Argument parsing.
#[derive(Clap, Debug)]
#[clap(author, about, version)]
struct Options {
    /// Symbols to fetch
    #[clap(short, long, default_value = "AAPL,MSFT,UBER,GOOG", parse(from_str = parse_symbols))]
    symbols: Vec<Vec<String>>, // TODO: Bug in clap adding extra unnecessary Vec?
    /// Start date
    #[clap(short, long)]
    from: DateTime<Utc>,
}

/// Split symbols into a list.
fn parse_symbols(symbols: &str) -> Vec<String> {
    symbols.split(',').map(|s| s.to_string()).collect()
}

/// Summary struct for one symbol.
struct Summary {
    period_start: DateTime<Utc>,
    symbol: String,
    last_price: f64,
    change_percentage: f64,
    period_min: f64,
    period_max: f64,
    average: f64,
}

impl Summary {
    /// Output summary in CSV-format.
    fn csv(&self) -> String {
        format!(
            "{},{},${:.2},{:.2}%,${:.2},${:.2},${:.2}",
            self.period_start.to_rfc3339(),
            self.symbol,
            self.last_price,
            self.change_percentage,
            self.period_min,
            self.period_max,
            self.average
        )
    }
}

/// Load data for summary.
fn load_summary(provider: &YahooConnector, options: &Options) -> Result<Vec<Summary>, YahooError> {
    let mut summaries = Vec::new();
    for symbol in options.symbols[0].iter() {
        let response = provider.get_quote_range(symbol, "1d", "30d")?;
        let quotes = response.quotes()?;
        let max_price = quotes
            .iter()
            .fold(0.0, |acc, v| if acc < v.high { v.high } else { acc });
        let min_price = quotes.iter().fold(
            f64::INFINITY,
            |acc, v| if acc > v.low { v.low } else { acc },
        );
        // Default to 0.0 if first or last price cannot be found.
        let first_price = quotes.first().map(|q| q.open).unwrap_or(0.0);
        let last_price = quotes.last().map(|q| q.close).unwrap_or(0.0);
        let change_percentage = (last_price - first_price) / first_price * 100.0;
        // Sum values and then dividing is not really numerically stable, but it works for now.
        let average = quotes.iter().fold(0.0, |acc, v| acc + v.close) / quotes.len() as f64;

        let summary = Summary {
            period_start: options.from,
            symbol: symbol.clone(),
            last_price,
            change_percentage,
            period_min: min_price,
            period_max: max_price,
            average,
        };
        summaries.push(summary);
    }
    Ok(summaries)
}

fn run() -> Result<(), String> {
    let options: Options = Options::parse();
    let provider = yahoo::YahooConnector::new();
    let summaries = match load_summary(&provider, &options) {
        Ok(s) => s,
        // Translate the error into a string.
        Err(e) => return Err(e.to_string()),
    };
    println!("period start,symbol,price,change %,min,max,30d avg");
    for summary in summaries {
        println!("{}", summary.csv());
    }
    Ok(())
}

fn main() {
    if let Err(e) = run() {
        eprintln!("{}", e)
    };
}
